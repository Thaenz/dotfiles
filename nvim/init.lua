--[[ plugins ]]----------------------------------------------------------------
-- https://github.com/faerryn/user.nvim
local user = require "user"
user.setup { parallel = false }
local use = user.use
 
--use {
--	-- [[ Multi cursor ]]
--	"mg979/vim-visual-multi",
--}

use {
	-- [[ Lightweight commenter ]]
	"terrortylor/nvim-comment",
	config = function()
		require('nvim_comment').setup()
	end,
}

use {
	-- [[ Based nord colorscheme ]]
	"arcticicestudio/nord-vim",
	config = function()
		-- don't set colorscheme in tty
		if os.getenv("DISPLAY") then
			vim.api.nvim_command "colorscheme nord"
		end
	end,
}

use {
	-- [[ Lightweight statusline ]]
	"nvim-lualine/lualine.nvim",
	config = function()
		local o = { }
		o.theme                = "nord"
		o.icons_enabled        = false
		o.component_separators = '|'
		o.section_separators   = ''

		require "lualine".setup { options = o }
	end,
}

--use {
--	-- [[ Better hightlight system ]]
--	"nvim-treesitter/nvim-treesitter",
--	config = function()
--		local o = { }
--		o.ensure_installed = "maintained"
--		o.sync_install     = false
--		o.ignore_install   = { "rust" }
--
--		o.highlight = { }
--		o.highlight.enable = true
--		o.highlight.additional_vim_regex_highlighting = true
--
--		require "nvim-treesitter.configs".setup(o)
--	end,
--}

user.flush()

--[[ options ]]----------------------------------------------------------------

local o = vim.o

o.termguicolors = true -- https://unix.stackexchange.com/a/404523
o.colorcolumn   = "80"
o.cursorline    = true
o.splitbelow    = true
o.splitright    = true
o.ignorecase    = true
o.laststatus    = 2
o.smartcase     = true
o.fillchars     = "eob: "
o.showmode      = false
o.swapfile      = false
o.wrap          = false

--[[ keymaps ]]----------------------------------------------------------------

local noremap = { noremap = true }
local snoremap = { silent= true, noremap = true }
local map = vim.api.nvim_set_keymap

-- remove highlight on escape
map('n', "<Esc>", ":nohl<CR><Esc>", snoremap)

-- auto expand brackets
map('i', "{<CR>", "{}<ESC>i<CR><ESC>O", noremap)

-- keep visual mode when indenting
map('v', '<', "<gv", noremap)
map('v', '>', ">gv", noremap)

-- keeps last register when pasting
map('v', 'p', '"_dP', noremap)
map('v', 'P', '"_dP', noremap)

-- quick substitute
map('v', 's', ':s/', noremap)

-- commenting
map('n', 'g#', ':s/^/\\#/<CR>', snoremap)
map('v', 'g#', ':s/^/\\#/<CR>', snoremap)
map('n', 'g/', ':s/^/\\/\\//<CR>', snoremap)
map('v', 'g/', ':s/^/\\/\\//<CR>', snoremap)
map('n', 'g-', ':s/^/--/<CR>', snoremap)
map('v', 'g-', ':s/^/--/<CR>', snoremap)

