if [ "$TERM" = "linux" ]; then
	# Normal colors
	printf '%b' '\e]P03B4252' #black
	printf '%b' '\e]P9BF616A' #red
	printf '%b' '\e]PAA3BE8C' #green
	printf '%b' '\e]PBEBCB8B' #yellow
	printf '%b' '\e]PC81A1C1' #blue
	printf '%b' '\e]PDB48EAD' #magenta
	printf '%b' '\e]PE88C0D0' #cyan
	printf '%b' '\e]PFE5E9F0' #white

	# Bright colors
	printf '%b' '\e]P84B166A' #black
	printf '%b' '\e]P1BF616A' #red
	printf '%b' '\e]P2A3BE8C' #green
	printf '%b' '\e]P3EBCB8B' #yellow
	printf '%b' '\e]P481A1C1' #blue
	printf '%b' '\e]P5B48EAD' #magenta
	printf '%b' '\e]P68FBCBB' #cyan
	printf '%b' '\e]P7ECEFF4' #white
fi
